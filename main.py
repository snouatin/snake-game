from turtle import *
import time
import random

score = 0
execution_delay=0.1

# Creation of a screen

root=Screen()
root.title('Snake Game')
root.setup(width=600,height=600)
root.bgcolor('white')
root.bgpic('./resources/border.gif')
root.tracer(False)
root.addshape('./resources/upmouth.gif')
root.addshape('./resources/food.gif')
root.addshape('./resources/upmouth.gif')
root.addshape('./resources/downmouth.gif')
root.addshape('./resources/leftmouth.gif')
root.addshape('./resources/rightmouth.gif')
root.addshape('./resources/body.gif')

# Creation of the snake

head = Turtle()
head.shape("./resources/upmouth.gif") # head of the snake
head.penup() # to remove line while moving
head.goto(0, 0)
head.direction = 'stop'

# Creation of the food

food = Turtle()
food.shape('./resources/food.gif')
food.penup()
food.goto(0, 100)

# To display text on the screen

text = Turtle()
text.penup()
text.goto(0, 266)
text.hideturtle()
text.color("White")
text.write("Score:0", font = ('courier', 25, 'bold'), align = 'center')

# Game over text
lost=Turtle()
lost.color('black')
lost.penup()
lost.hideturtle()

# To move the snake
def move_snake():
    if head.direction == "up":
        y = head.ycor()
        y += 20
        head.sety(y)

    if head.direction == "down":
        y = head.ycor()
        y -= 20
        head.sety(y)

    if head.direction=='right':
        x=head.xcor()
        x += 20
        head.setx(x)

    if head.direction=='left':
        x=head.xcor()
        x -= 20
        head.setx(x)

def go_up():
    if head.direction!='down':
        head.direction='up'
        head.shape('./resources/upmouth.gif')

def go_down():
    if head.direction!='up':
        head.direction='down'
        head.shape('./resources/downmouth.gif')


def go_left():
    if head.direction!='right':
        head.direction='left'
        head.shape('./resources/leftmouth.gif')


def go_right():
    if head.direction!='left':
        head.direction='right'
        head.shape('./resources/rightmouth.gif')


# linking keyboard keys to functions

root.listen()

root.onkeypress(go_up,'Up')
root.onkeypress(go_down,'Down')
root.onkeypress(go_left,'Left')
root.onkeypress(go_right,'Right')
segments=[]

while True:
    root.update()

    if abs(head.xcor()) > 260 or abs(head.ycor()) > 260:
        lost.write('Game Over',align='center',font=('courier',32,'bold'))
        time.sleep(1)
        
        lost.clear()
        time.sleep(1)
        
        head.goto(0,0)
        head.direction='stop'
        
        for bodies in segments:
            bodies.goto(1000,1000)
        score = 0
        execution_delay = 0.1
        segments.clear()
        text.clear()
        text.write('Score:0', align='center',font=('courier', 25,'bold'))

    if head.distance(food) < 20:
        x = random.randint(-255,255)
        y = random.randint(-255,255)
        food.goto(x,y)
        execution_delay -= 0.002

        body=Turtle()
        body.penup()
        body.shape('./resources/body.gif')
        segments.append(body)

        score += 10
        text.clear()
        text.write(f'Score:{score}', font=('courier',25,'bold'), align='center')

# How snake grow
    for i in range(len(segments)-1,0,-1):
        x=segments[i-1].xcor()
        y=segments[i-1].ycor()
        segments[i].goto(x,y)

    if len(segments)>0:
        x=head.xcor()
        y=head.ycor()
        segments[0].goto(x,y)    
    
    move_snake()

    for bodies in segments:
        if bodies.distance(head)<20:
            time.sleep(1)
            head.goto(0,0)
            head.direction='stop'

            for bodies in segments:
                bodies.goto(1000,1000)

            segments.clear()
            score=0
            execution_delay=0.1
            lost.write('Game Over', align='center', font=('courier', 32, 'bold'))
            time.sleep(1)
            lost.clear()

            text.clear()
            text.write('Score:0', align='center', font=('courier', 25, 'bold'))

    time.sleep(execution_delay)