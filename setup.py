from cx_Freeze import *
import sys
includefiles=['./resources/icon.ico','./resources/body.gif','./resources/border.gif','./resources/downmouth.gif','./resources/upmouth.gif',
            './resources/leftmouth.gif', './resources/rightmouth.gif','./resources/food.gif']

base=None
if sys.platform=="win32":
    base="Win32GUI"

shortcut_table=[
    ("DesktopShortcut",
     "DesktopFolder",
     "Snake Game",
     "TARGETDIR",
     "[TARGETDIR]\snake.exe",
     None,
     None,
     None,
     None,
     None,
     None,
     "TARGETDIR",
     )
]
msi_data={"Shortcut":shortcut_table}

bdist_msi_options={'data':msi_data}
setup(
    version="0.1",
    description="Snake Game",
    author="Steve Nouatin",
    name="Snake Game",
    options={'build_exe':{'include_files':includefiles},'bdist_msi':bdist_msi_options,},
    executables=[
        Executable(
            script="main.py",
            base=base,
            icon='./resources/icon.ico',
        )
    ]
)
